# ansible_role_unifi_container




## Name
Ansible role to deploy a UniFi container onto a RHEL8 or RHEL9 system using Podman

## Installation

Run this role on a base installed RHEL system:

```
% ansible-playbook -i <destination-host>, container-unifi.yaml
```

## License
This role is GPLv3 licensed - feel free to use/copy/modify/distribute/*
